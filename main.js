var express = require('express')
var app = express(),
http = require("http");
var server = http.createServer(app);

app.use(express.static(__dirname + "/public" ));
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
  res.render('index')
})

app.get('/callback', function (req, res) {
  res.render('callback')
})

server.listen(6969, function () {
    console.log("\x1b[31m", "Server running at port " + 6969);
  });